#
# To learn more about a Podspec see http://guides.cocoapods.org/syntax/podspec.html.
# Run `pod lib lint flutterSmiSdkPlugin.podspec' to validate before publishing.
#
Pod::Spec.new do |s|
  s.name             = 'flutterSmiSdkPlugin'
  s.version          = '0.0.1'
  s.summary          = 'Datami Flutter Plugin'
  s.description      = <<-DESC
  Smi SDK for Sponsored Data on iOS Devices by Datami Mobile Solutions Pvt Ltd.
                       DESC
  s.homepage         = 'https://www.datami.com'

  s.license = { :type => 'EULA'}

  s.author           = { 'Datami Mobile Solutions Pvt Ltd' => 'Anil Kumar' }
  
  s.preserve_paths = 'SmiSdk.framework'
  s.xcconfig = { 'OTHER_LDFLAGS' => '-framework SmiSdk' }

  s.source           = { :path => '.' }

  s.source_files = 'Classes/*.{swift,h,m,c,cc,mm,cpp}'
  s.ios.deployment_target  = '9'
  s.vendored_frameworks = 'Classes/SmiSdk.framework'
    
  s.dependency 'Flutter'

  s.ios.frameworks   = [
    'CoreFoundation',
    'SystemConfiguration'
  ]


  # Flutter.framework does not contain a i386 slice.
  s.pod_target_xcconfig = { 'DEFINES_MODULE' => 'YES', 'EXCLUDED_ARCHS[sdk=iphonesimulator*]' => 'x86_64' }
end
