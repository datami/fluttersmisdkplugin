#import "FlutterSmiSdkPlugin.h"
#import "SmiSdk.framework/Headers/SmiSdk.h"

@implementation FlutterSmiSdkPlugin
+ (void)registerWithRegistrar:(NSObject<FlutterPluginRegistrar>*)registrar {
  FlutterMethodChannel* channel = [FlutterMethodChannel
      methodChannelWithName:@"com.datami.flutterSmiSdkPlugin/sdState"
            binaryMessenger:[registrar messenger]];
  FlutterSmiSdkPlugin* instance = [[FlutterSmiSdkPlugin alloc] init];
  instance.channel = channel;
  [registrar addMethodCallDelegate:instance channel:channel];
  [registrar addApplicationDelegate:instance];

}

-(BOOL) application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions{

    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(receivedStateChage:)
                                                 name:SDSTATE_CHANGE_NOTIF object:nil];
    
    return YES;
}

- (void)handleMethodCall:(FlutterMethodCall*)call result:(FlutterResult)result {
  if ([@"startSponsorData" isEqualToString:call.method]) {
      NSLog(@"Plugin startSponsorData() api called!");
      [SmiSdk startSponsorData];
      result(nil);
  }else if ([@"stopSponsorData" isEqualToString:call.method]) {
      NSLog(@"Plugin stopSponsorData() api called!");
      [SmiSdk stopSponsorData];
      result(nil);
  }else if([@"updateUserTag" isEqualToString:call.method]){
      NSLog(@"Plugin updateUserTag() api called!");
      [SmiSdk updateTag:call.arguments];
      result(nil);

  }else if([@"updateUserId" isEqualToString:call.method]){
      NSLog(@"Plugin updateUserId() api called!");
      [SmiSdk updateUserId:call.arguments];
      result(nil);

  }else if ([@"getPlatformVersion" isEqualToString:call.method]) {
    result([@"iOS " stringByAppendingString:[[UIDevice currentDevice] systemVersion]]);

  } else {
    result(FlutterMethodNotImplemented);
  }
}

-(void)receivedStateChage:(NSNotification*)notif {
    SmiResult* sr =  notif.object;
    NSString *state = [SmiSdk getSdStateString:(int)sr.sdState];
    NSString *sdReason = [SmiSdk getReasonString:(int)sr.sdReason];
    NSLog(@"[dmi]FlutterPlugin receivedStateChage, sdState: %@, sdReason %@", state, sdReason);
    NSDictionary *sdState = [NSDictionary dictionaryWithObjectsAndKeys:state, @"state", sdReason, @"reason", nil];
    
    [self.channel invokeMethod:@"onSdStateChange" arguments:sdState];
}

@end
