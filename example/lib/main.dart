import 'package:flutter/material.dart';
import 'dart:async';

import 'package:flutter/services.dart';
import 'package:flutterSmiSdkPlugin/flutterSmiSdkPlugin.dart';

void main() {
  runApp(MyApp());
}


class MyApp extends StatefulWidget {
  @override
  _MyAppState createState() => _MyAppState();
}

class _MyAppState extends State<MyApp> {
  String _platformVersion = 'Unknown';
  @override
  void initState() {
    super.initState();
    //Add SdStateListner to receive SD state
    FlutterSmiSdkPlugin.addSdStateListner((Map sdState){
      print("ANIL : $sdState");
    });

    initPlatformState();
  }

  // Platform messages are asynchronous, so we initialize in an async method.
  Future<void> initPlatformState() async {
    String platformVersion;
    // Platform messages may fail, so we use a try/catch PlatformException.
    try {
      platformVersion = await FlutterSmiSdkPlugin.platformVersion;
    } on PlatformException {
      platformVersion = 'Failed to get platform version.';
    }

    // If the widget was removed from the tree while the asynchronous platform
    // message was in flight, we want to discard the reply rather than calling
    // setState to update our non-existent appearance.
    if (!mounted) return;

    setState(() {
      _platformVersion = platformVersion;
    });
  }

  Future<void> _startSponsoredData() async {
    try {
      FlutterSmiSdkPlugin.startSponsorData();
    } on PlatformException catch (e) {
      print(e);
    }
    if (!mounted) return;

  }

  Future<void> _stopSponsoredData() async {
    try {
      FlutterSmiSdkPlugin.stopSponsorData();
    } on PlatformException catch (e) {
      print(e);
    }
    if (!mounted) return;

  }
  Future<void> _updateUserId() async {
    try {
      String user = "Anil kumar";
      FlutterSmiSdkPlugin.updateUserId(user);
    } on PlatformException catch (e) {
      print(e);
    }
    if (!mounted) return;

  }

  Future<void> _updateUserTag() async {
    try {
      List<String> tags = new List<String>();
      tags.add("gold");
      tags.add("silver");
      FlutterSmiSdkPlugin.updateTag(tags);
    } on PlatformException catch (e) {
      print(e);
    }
    if (!mounted) return;

  }

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
        home: Scaffold(
          appBar: AppBar(
            title: const Text('Datami Plugin example app'),
          ),
          body: ConstrainedBox(
              constraints: const BoxConstraints.expand(),
              child: Column(
                  mainAxisAlignment: MainAxisAlignment.spaceAround,
                  children: <Widget>[
                    RaisedButton(
                      child: const Text('startSponsoredData()'),
                      onPressed: _startSponsoredData,
                    ),
                    RaisedButton(
                      child: const Text('stopSponsoredData()'),
                      onPressed: _stopSponsoredData,
                    ),
                    RaisedButton(
                      child: const Text('updateUserId()'),
                      onPressed: _updateUserId,
                    ),
                    RaisedButton(
                      child: const Text('updateUserTag()'),
                      onPressed: _updateUserTag,
                    )
                  ])),
        ));
  }
}
