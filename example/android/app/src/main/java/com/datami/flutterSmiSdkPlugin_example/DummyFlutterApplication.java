package com.datami.flutterSmiSdkPlugin_example;


import com.datami.flutterSmiSdkPlugin.FlutterSmiSdkPlugin;

import io.flutter.app.FlutterApplication;

public class DummyFlutterApplication extends FlutterApplication {
    @Override
    public void onCreate() {
        super.onCreate();
        FlutterSmiSdkPlugin.initialiseDatamiSdk(getString(R.string.api_key), this, "", R.mipmap.ic_launcher,
                getString(R.string.sdk_messaging).equalsIgnoreCase("true"));
    }
}
