#import "AppDelegate.h"
#import "GeneratedPluginRegistrant.h"
#import <SmiSdk/SmiSdk.h>

@implementation AppDelegate

- (BOOL)application:(UIApplication *)application
    didFinishLaunchingWithOptions:(NSDictionary *)launchOptions {
  [GeneratedPluginRegistrant registerWithRegistry:self];
  // Override point for customization after application launch.
    [SmiSdk initSponsoredData:@"apikey" userId:@"anil.kumar@datamin.com" showSDMessage:YES];

  return [super application:application didFinishLaunchingWithOptions:launchOptions];
}

@end
