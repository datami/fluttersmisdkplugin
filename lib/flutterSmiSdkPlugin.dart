
import 'dart:async';

import 'package:flutter/services.dart';
typedef void OnSdStateChange(Map sdState);

class FlutterSmiSdkPlugin {
  static const String CHANNEL_ID = "com.datami.flutterSmiSdkPlugin/sdState";
  static const MethodChannel _channel =
      const MethodChannel(CHANNEL_ID);

  static void addSdStateListner(OnSdStateChange handler) async {
    _channel.setMethodCallHandler((MethodCall call) async {
      if("onSdStateChange" == call.method && call.arguments is Map){
        handler(call.arguments);
      }
    });
  }

  static Future<String> get platformVersion async {
    final String version = await _channel.invokeMethod('getPlatformVersion');
    return version;
  }

  static Future<bool> startSponsorData() async {
    try {
      await _channel.invokeMethod('startSponsorData');
    } on PlatformException catch (e) {
      print(e.message);
      return false;
    }
    return true;
  }

  static Future<bool> stopSponsorData() async {
    try {
      await _channel.invokeMethod('stopSponsorData');
    } on PlatformException catch (e) {
      print(e.message);
      return false;
    }
    return true;
  }

  static Future<bool> updateUserId(String userId) async {
    try {
      await _channel.invokeMethod('updateUserId', userId);
    } on PlatformException catch (e) {
      print(e.message);
      return false;
    }
    return true;
  }

  static Future<bool> updateTag(List<String> userTags) async {
    try {
      await _channel.invokeMethod('updateUserTag', userTags);
    } on PlatformException catch (e) {
      print(e.message);
      return false;
    }
    return true;
  }

}
