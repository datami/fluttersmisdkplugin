package com.datami.flutterSmiSdkPlugin;

import android.content.Context;
import android.text.TextUtils;
import android.util.Log;

import java.util.Arrays;
import java.util.List;

import io.flutter.app.FlutterApplication;

public class FlutterDatamiApplication extends FlutterApplication  {
    private static String TAG = "FlutterDatamiApp";

    @Override
    public void onCreate() {
        super.onCreate();
        Context context = getApplicationContext();

        int resApiKey = context.getResources().getIdentifier("api_key", "string", context.getPackageName());
        int resSdkMess = context.getResources().getIdentifier("sdk_messaging", "string", context.getPackageName());
        int resIcFolder = context.getResources().getIdentifier("icon_folder", "string", context.getPackageName());
        int resIcName = context.getResources().getIdentifier("icon_name", "string", context.getPackageName());

        int resUserId = context.getResources().getIdentifier("smisdk_user_id", "string", context.getPackageName());
        int resExcluDomains = context.getResources().getIdentifier("smisdk_exclusive_domains", "array", context.getPackageName());
        int resUserTag = context.getResources().getIdentifier("smisdk_user_tags", "array", context.getPackageName());
        int resStartProxy = context.getResources().getIdentifier("smisdk_start_proxy", "string", context.getPackageName());

        String apiKey = context.getResources().getString(resApiKey);
        if(apiKey == null){
            apiKey = "noapikeyspecified";
        }
        String useSdkMessaging = context.getResources().getString(resSdkMess);
        if(useSdkMessaging == null) {
            useSdkMessaging = "false";
        }
        String iconFolder = null;
        String iconName = null;

        if(resIcFolder != 0 && resIcName !=  0) {
            iconFolder = context.getResources().getString(resIcFolder);
            iconName = context.getResources().getString(resIcName);
        }
        if(iconFolder == null) {
            iconFolder = "mipmap";
        }
        if(iconName == null) {
            iconName = "ic_launcher";
        }

        String userID = "";
        if(resUserId!=0){
            String usId = context.getResources().getString(resUserId);
            if(!TextUtils.isEmpty(usId)){
                userID = usId;
            }
        }

        List<String> exclusionDomain = null;
        if(resExcluDomains!=0){
            String[] exDomainArray = context.getResources().getStringArray(resExcluDomains);
            if(exDomainArray!=null && exDomainArray.length>0){
                exclusionDomain = Arrays.asList(exDomainArray);
            }
        }

        List<String> userTags = null;
        if(resUserTag!=0){
            try{
                String[] userTagsArray = context.getResources().getStringArray(resUserTag);
                if(userTagsArray!=null && userTagsArray.length>0){
                    userTags = Arrays.asList(userTagsArray);
                }
            }catch (Exception ex){
                ex.printStackTrace();
            }
        }

        boolean startProxy = true;
        if(resStartProxy!=0){
            try{
                String startProxyStr = context.getResources().getString(resStartProxy);
                if(!TextUtils.isEmpty(startProxyStr) && startProxyStr.equalsIgnoreCase("false")){
                    startProxy = false;
                }
            }catch (Exception ex){
                ex.printStackTrace();
            }
        }
        Log.d(TAG, "userID: " + userID + ", exclusionDomain: " + exclusionDomain + ", userTags: " + userTags + ", startProxy: " + startProxy);

        boolean sdkMessaging = Boolean.parseBoolean(useSdkMessaging);
        Log.d(TAG, "iconFolder: " + iconFolder + ", iconName: " + iconName);
        int iconId = context.getResources().getIdentifier(iconName, iconFolder, context.getPackageName());
        Log.d(TAG, "iconId: " + iconId);

        FlutterSmiSdkPlugin.initialiseDatamiSdk(apiKey, this, userID, iconId, sdkMessaging,
                exclusionDomain, userTags, startProxy);
    }
}
