package com.datami.flutterSmiSdkPlugin;

import android.content.Context;
import android.os.Handler;
import android.os.Looper;
import android.text.TextUtils;
import android.util.Log;

import androidx.annotation.NonNull;

import com.datami.smi.SdStateChangeListener;
import com.datami.smi.SmiResult;
import com.datami.smi.SmiSdk;

import java.util.HashMap;
import java.util.List;

import io.flutter.embedding.engine.plugins.FlutterPlugin;
import io.flutter.plugin.common.MethodCall;
import io.flutter.plugin.common.MethodChannel;
import io.flutter.plugin.common.MethodChannel.MethodCallHandler;
import io.flutter.plugin.common.MethodChannel.Result;

/** FlutterSmiSdkPlugin */
public class FlutterSmiSdkPlugin implements FlutterPlugin, MethodCallHandler {
  /// The MethodChannel that will the communication between Flutter and native Android
  ///
  /// This local reference serves to register the plugin with the Flutter Engine and unregister it
  /// when the Flutter Engine is detached from the Activity
  private static final String TAG = "[dmi]FLTRSmiSdkPlugin";
  private final String CHANNEL_ID = "com.datami.flutterSmiSdkPlugin/sdState";
  private static MethodChannel sChannel;

  @Override
  public void onAttachedToEngine(@NonNull FlutterPluginBinding flutterPluginBinding) {
    Log.i(TAG, "onAttachedToEngine");
    sChannel = new MethodChannel(flutterPluginBinding.getBinaryMessenger(), CHANNEL_ID);
    sChannel.setMethodCallHandler(this);
  }

  @Override
  public void onMethodCall(@NonNull MethodCall call, @NonNull Result result) {
    if (call.method.equals("startSponsorData")) {
      Log.i(TAG, "startSponsorData() api call");
      try{
        SmiSdk.startSponsoredData();
      }catch(Exception e){
        e.printStackTrace();
      }
    }else if (call.method.equals("stopSponsorData")) {
      Log.i(TAG, "stopSponsorData() api call");
      SmiSdk.stopSponsoredData();

    }else if (call.method.equals("updateUserTag")) {
      Log.i(TAG, "updateUserTag() api call");
      SmiSdk.updateUserTag((List<String>)call.arguments);

    }else if (call.method.equals("updateUserId")) {
      Log.i(TAG, "updateUserId() api call");
      SmiSdk.updateUserId((String) call.arguments);

    }else if (call.method.equals("getPlatformVersion")) {
      result.success("Android " + android.os.Build.VERSION.RELEASE);

    } else {
      result.notImplemented();
    }
  }

  public static void initialiseDatamiSdk(String apiKey, Context ctx, String userId, int iconId, boolean messaging){
    SmiSdk.initSponsoredData(apiKey, ctx, userId, iconId, messaging);
    addSdStateChangeListener();
  }

  public static void initialiseDatamiSdk(String apiKey, Context ctx, String userId, int iconId, boolean messaging,
                                         List<String> exclusionDomain, List<String> userTags, boolean startProxy){
    SmiSdk.initSponsoredData(apiKey, ctx, userId, iconId, messaging, exclusionDomain, userTags, startProxy);
    addSdStateChangeListener();
  }

  private static void addSdStateChangeListener(){
    SmiSdk.addSdStateChangeListener(new SdStateChangeListener() {
      @Override
      public void onChange(SmiResult smiResult) {
        String sdStateStr = "";
        String sdReasonStr = "";
        if(smiResult!=null){
          if(smiResult.getSdState()!=null && (!TextUtils.isEmpty(smiResult.getSdState().name()))){
            sdStateStr = smiResult.getSdState().name();
          }
          if(smiResult.getSdReason()!=null && (!TextUtils.isEmpty(smiResult.getSdReason().name()))){
            sdReasonStr = smiResult.getSdReason().name();
          }
        }else{
          Log.i(TAG,"smiResult is null : ");
        }
        Log.d("[dmi]Plugin SDState","State : "+sdStateStr+" Reason : "+sdReasonStr);
        final HashMap<String, String>sdState = new HashMap<>();
        sdState.put("state", sdStateStr);
        sdState.put("reason", sdReasonStr);
        Handler handler = new Handler(Looper.getMainLooper());
        handler.post(
            new Runnable() {
              @Override
              public void run() {
                if(sChannel!=null){
                  sChannel.invokeMethod("onSdStateChange", sdState);
                }else{
                  Log.i(TAG,"MethodChannel is null.");
                }
              }
            });
      }
    });
  }

  @Override
  public void onDetachedFromEngine(@NonNull FlutterPluginBinding binding) {
    if(sChannel!=null){
      sChannel.setMethodCallHandler(null);
    }
  }
}
